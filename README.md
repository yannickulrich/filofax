## Printing

To print this on A4 paper, do the following

Create the odd pages, i.e. change the main loop to
``` 
16 {
 makepage
 14 adddays
 PAGENUM 1 add /PAGENUM exch def
}repeat
```
and execute
```
gs -dDEVICEWIDTHPOINTS=252 -dDEVICEHEIGHTPOINTS=396  -dSAFER  -dNOPAUSE -dBATCH -sDEVICE=pdfwrite  -sOutputFile=page-odd.pdf    page.ps
```
then swap, i.e.
``` 
16 {
 14 adddays
 PAGENUM 1 add /PAGENUM exch def
 makepage
}repeat
```
and execute
```
gs -dDEVICEWIDTHPOINTS=252 -dDEVICEHEIGHTPOINTS=396  -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite  -sOutputFile=page-even.pdf    page.ps
```
Open `page-odd.pdf` with evince and under Page Setup choose

 * Two-sided: `One Sided`
 * Pages per side: `4`
 * Page ordering: `Left to right, top to bottom`
 * Only print: `All sheets`
 * Scale: `210%`

Execute the print job. For the even pages, put the odd pages in order
(Jan-Dec) into the manual feed so that you can read the January page
and choose
 * Page ordering: `Rigth to left, top to bottom`


## Cutting instructions

1) Cut the pages in half vertically in two thin strips. Set the
   machine to `105mm`

2) Cut the right margin of the left strip by setting to `100mm`

3) Cut the right margin of the right strip at `95mm`

4) Cut the left margin of the left strip at `93mm`

5) There is no need to cut the left margin of the right strip yet

6) Cut the strips horizontally in half at `145mm`

7) Cut the top margin of the top page at `140mm` (5.5in)

8) Cut the bottom margin of the bottom page at `140mm` (5.5in)

9) Sort the pages and, on the left margin of the top page, make at
mark in the centre (2.75in)
